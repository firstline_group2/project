"""
Decision Tree Classification Example.
"""

from __future__ import print_function


from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('ml-bank').getOrCreate()
df = spark.read.load("d:/TestTable.csv",
                     format="csv", sep=";", inferSchema="true", header="true")
df.printSchema()
df.show()
df = df.select('drug','drug_time','drug_gap','proc','stage')
cols = df.columns
from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, VectorAssembler

categoricalColumns = ['drug','proc']
stages = []
for categoricalCol in categoricalColumns:
    stringIndexer = StringIndexer(inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
    encoder = OneHotEncoderEstimator(inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
    stages += [stringIndexer, encoder]
label_stringIdx = StringIndexer(inputCol = 'stage', outputCol = 'label')
stages += [label_stringIdx]
numericCols = ['drug_time','drug_gap']
assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericCols
assembler = VectorAssembler(inputCols=assemblerInputs, outputCol="features")
stages += [assembler]
from pyspark.ml import Pipeline
pipeline = Pipeline(stages = stages)
pipelineModel = pipeline.fit(df)
df = pipelineModel.transform(df)
selectedCols = ['label', 'features'] + cols
df = df.select(selectedCols)
df.printSchema()
df.show()
train, test = df.randomSplit([0.7, 0.3], seed = 2018)

# from pyspark.ml.classification import LogisticRegression
# lr = LogisticRegression(featuresCol = 'features', labelCol = 'label', maxIter=10)
# lrModel = lr.fit(train)
# predictions = lrModel.transform(test)
# predictions.select('drug', 'drug_time', 'label', 'rawPrediction', 'prediction', 'probability').show(30)
#
#
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
# evaluator = MulticlassClassificationEvaluator(predictionCol="prediction")
# print("evaluator: " + str(evaluator.evaluate(predictions)))
#


from pyspark.ml.classification import RandomForestClassifier, RandomForestClassificationModel
rf = RandomForestClassifier(featuresCol = 'features', labelCol = 'label', numTrees=10)
rfModel = rf.fit(train)
rfModel.write().overwrite().save("D:/rfc_model")
predictions = rfModel.transform(test)
predictions.select('drug', 'drug_time', 'label', 'rawPrediction', 'prediction', 'probability').show(30)
evaluator = MulticlassClassificationEvaluator(predictionCol="prediction")
print("evaluator: " + str(evaluator.evaluate(predictions)))
model3 = RandomForestClassificationModel.load("D:/rfc_model")
predictions = model3.transform(test)
predictions.select('drug', 'drug_time', 'label', 'rawPrediction', 'prediction', 'probability').show(30)
evaluator = MulticlassClassificationEvaluator(predictionCol="prediction")
