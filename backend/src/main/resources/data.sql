insert into users (username, password, email)
            values ('example1', '$2a$10$fkJ993Z5vQVW5WqqVUTSQOcDWFPQVvikwrVpMUl2XBufe6VzRsHFy', 'test@gmail.com');
insert into users (username, password, email)
            values ('example2', '$2a$10$fkJ993Z5vQVW5WqqVUTSQOcDWFPQVvikwrVpMUl2XBufe6VzRsHFy', 'admin@gmail.com');
