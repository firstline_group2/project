create table drug_intake
(
    id         BIGINT,
    date_start DATE,
    date_end   DATE,
    patient_id BIGINT,
    drug varchar(255),
    dose varchar(255)
);

create table diagnose
(
    id         BIGINT,
    date_start DATE,
    patient_id BIGINT,
    diagnose   varchar(255)
);

create table med_procedure
(
    id         BIGINT,
    date_start DATE,
    date_end   DATE,
    patient_id BIGINT,
    med_procedure varchar(255)

);

CREATE TABLE users
(
	id_user int primary key auto_increment,
    username VARCHAR(45) unique not null,
    password VARCHAR(60) not null,
    email varchar(255)
);

create table role
(
    role 	varchar(255) primary key unique not null
);

create table user_role
(
	id_user 	int not null,
    role		varchar(255) not null,
    foreign key (id_user) references users(id_user)
        on delete cascade,
	foreign key (role) references role(role)
		on update cascade
        on delete cascade
);