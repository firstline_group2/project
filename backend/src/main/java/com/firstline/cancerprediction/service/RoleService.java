package com.firstline.cancerprediction.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
    List<GrantedAuthority> getRoles();

    void addRoleByIdUser(long id, GrantedAuthority grantedAuthority);

    void addRoleByUsername(String username, GrantedAuthority grantedAuthority);

    void deleteRolesByIdUser(long id);
}
