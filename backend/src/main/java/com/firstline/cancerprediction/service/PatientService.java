package com.firstline.cancerprediction.service;

import com.firstline.cancerprediction.entity.DiagnoseEntity;
import com.firstline.cancerprediction.entity.DrugEntity;
import com.firstline.cancerprediction.entity.ProcedureEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PatientService {
    List<DrugEntity> getDrugByPatientId(long patientId);

    List<DiagnoseEntity> getDiagnoseByPatientId(long patientId);

    List<ProcedureEntity> getProcedureByPatientId(long patientId);

}
