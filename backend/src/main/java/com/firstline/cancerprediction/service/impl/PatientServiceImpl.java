package com.firstline.cancerprediction.service.impl;

import com.firstline.cancerprediction.entity.DiagnoseEntity;
import com.firstline.cancerprediction.entity.DrugEntity;
import com.firstline.cancerprediction.repository.hive.DiagnoseRepository;
import com.firstline.cancerprediction.entity.ProcedureEntity;
import com.firstline.cancerprediction.repository.hive.DrugRepository;
import com.firstline.cancerprediction.repository.hive.ProcedureRepository;
import com.firstline.cancerprediction.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private DrugRepository drugRepository;

    @Autowired
    private ProcedureRepository procedureRepository;

    @Autowired
    private DiagnoseRepository diagnoseRepository;

    @Override
    public List<DrugEntity> getDrugByPatientId(long patientId) {
        return drugRepository.getDrugEntitiesByPatientId(patientId);
    }

    @Override
    public List<DiagnoseEntity> getDiagnoseByPatientId(long patientId) {
        return diagnoseRepository.getDiagnoseEntitiesByPatientId(patientId);
    }

    @Override
    public List<ProcedureEntity> getProcedureByPatientId(long patientId) {
        return procedureRepository.getProcedureEntitiesByPatientId(patientId);
    }
}
