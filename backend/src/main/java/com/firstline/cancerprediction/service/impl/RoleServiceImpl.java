package com.firstline.cancerprediction.service.impl;

import com.firstline.cancerprediction.repository.mysql.RoleRepository;
import com.firstline.cancerprediction.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public List<GrantedAuthority> getRoles() {
        return roleRepository.getRoles();
    }

    @Override
    public void addRoleByIdUser(long id, GrantedAuthority grantedAuthority) {
        roleRepository.addRoleByIdUser(id, grantedAuthority);
    }

    @Override
    public void addRoleByUsername(String username, GrantedAuthority grantedAuthority) {
        roleRepository.addRoleByUsername(username, grantedAuthority);
    }

    @Override
    public void deleteRolesByIdUser(long id) {
        roleRepository.deleteRolesByIdUser(id);
    }
}
