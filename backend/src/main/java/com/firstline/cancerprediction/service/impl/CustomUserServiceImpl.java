package com.firstline.cancerprediction.service.impl;

import com.firstline.cancerprediction.entity.CustomUser;
import com.firstline.cancerprediction.entity.UserEntity;
import com.firstline.cancerprediction.repository.mysql.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomUserServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public CustomUser loadUserByUsername(String username) {
        UserEntity userEntity = userRepository.getUserEntityByUsername(username);

        return new CustomUser(userEntity);
    }

    public UserEntity getUserEntityByIdUser(long id) {
        return userRepository.getUserEntityByIdUser(id);
    }

    public long getIdUserByUsername(String username){
        return userRepository.getIdUserByUsername(username);
    }

    public List<UserEntity> getUserEntities() {
        return userRepository.getUserEntities();
    }

    public void addUserEntity(UserEntity userEntity) {
        userRepository.addUserEntity(userEntity);
    }

    public void updateUserFieldByIdUser(String field, String value, long id) {
        userRepository.updateUserFieldByIdUser(field, value, id);
    }

    public void deleteUserByIdUser(long id) {
        userRepository.deleteUserByIdUser(id);
    }
}
