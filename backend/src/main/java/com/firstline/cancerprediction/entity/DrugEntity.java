package com.firstline.cancerprediction.entity;

import com.firstline.cancerprediction.entity.common.Date;
import lombok.Data;



@Data
public class DrugEntity {

    private long id;
    private Date dateStart;
    private Date dateEnd;
    private long patientId;
    private String drug;
    private String dose;
}
