package com.firstline.cancerprediction.entity.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Data
@JsonIgnoreProperties({"date"})
public class Date {
    private String date;
    private int year;
    private int month;
    private int day;

    public Date(String date) {
        this.date = date;

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

        java.util.Date parsingDate = new java.util.Date();

        try {
            parsingDate = ft.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(parsingDate.getTime());

        year = gregorianCalendar.get(Calendar.YEAR);
        month = gregorianCalendar.get(Calendar.MONTH) + 1;
        day = gregorianCalendar.get(Calendar.DAY_OF_MONTH);
    }
}

