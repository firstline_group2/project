package com.firstline.cancerprediction.entity;

import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {

    private static final long serialVersionUID = 1L;

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    public CustomUser(UserEntity userEntity) {
        super(
                userEntity.getUsername(),
                userEntity.getPassword(),
                userEntity.getGrantedAuthoritiesSet());
    }

}
