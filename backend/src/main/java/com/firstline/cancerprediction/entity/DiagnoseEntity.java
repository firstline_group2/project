package com.firstline.cancerprediction.entity;

import com.firstline.cancerprediction.entity.common.Date;
import lombok.Data;



@Data
public class DiagnoseEntity {

    private long id;
    private Date dateStart;
    private Date dateEnd;
    private long patientId;
    private String diagnose;
}
