package com.firstline.cancerprediction.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.LinkedHashSet;
import java.util.Set;

@Data
public class UserEntity {
    private long id;
    private String username;
    private String password;
    private String email;
    private Set<GrantedAuthority> grantedAuthoritiesSet = new LinkedHashSet<>();
}
