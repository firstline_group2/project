package com.firstline.cancerprediction.api.v1.validator;

import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    @Override
    public void initialize(ValidPassword constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        LengthComplexityRule lengthComplexityRule = new LengthComplexityRule();
        lengthComplexityRule.addRules("[0,0]", new WhitespaceRule());
        lengthComplexityRule.addRules("[8,30]", new WhitespaceRule(), new CharacterRule(EnglishCharacterData.Digit, 1));
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                lengthComplexityRule
        ));

        RuleResult result = validator.validate(new PasswordData(value));
        if (result.isValid()){
            return true;
        }

        List<String> messages = validator.getMessages(result);

        String messageTemplate = messages.stream().collect(Collectors.joining(","));

        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
        return false;
    }
}
