package com.firstline.cancerprediction.api.v1.dto;

import com.firstline.cancerprediction.api.v1.validator.ValidPassword;
import com.firstline.cancerprediction.entity.UserEntity;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.Email;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
public class UserDTO {

    private long id;
    private String username;
    @Email
    private String email;
    @ValidPassword
    private String password;
    private Set<String> grantedAuthoritiesSet = new LinkedHashSet<>();

    public static UserDTO getUserDTO(UserEntity userEntity){
        UserDTO userDTO = new UserDTO();

        userDTO.setId(userEntity.getId());
        userDTO.setUsername(userEntity.getUsername());
        userDTO.setEmail(userEntity.getEmail());

        for (GrantedAuthority g: userEntity.getGrantedAuthoritiesSet()){
            userDTO.getGrantedAuthoritiesSet().add(g.getAuthority());
        }

        return userDTO;
    }
}
