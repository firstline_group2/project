package com.firstline.cancerprediction.api.v1.controller;

import com.firstline.cancerprediction.api.v1.dto.UserDTO;
import com.firstline.cancerprediction.api.v1.dto.UsersRolesDTO;
import com.firstline.cancerprediction.service.impl.CustomUserServiceImpl;
import com.firstline.cancerprediction.service.impl.RoleServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@CrossOrigin()
@RequestMapping("/api/v1/admin")
public class AdminController {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private final CustomUserServiceImpl customUserService;

    @Autowired
    private final RoleServiceImpl roleService;

    @RequestMapping(value = "/setUserProfile", method = RequestMethod.POST)
    public void setUserProfile(@RequestParam long id, @RequestBody @Valid UserDTO userDTO) {
        if (userDTO.getPassword() != null && !userDTO.getPassword().equals("")) {
            customUserService.updateUserFieldByIdUser("password", passwordEncoder().encode(userDTO.getPassword()), id);
        }
        if (userDTO.getGrantedAuthoritiesSet() != null) {
            roleService.deleteRolesByIdUser(id);

            for (String s : userDTO.getGrantedAuthoritiesSet()) {
                SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(s);
                roleService.addRoleByIdUser(id, simpleGrantedAuthority);
            }
        }
        if (userDTO.getEmail() != null) {
            customUserService.updateUserFieldByIdUser("email", userDTO.getEmail(), id);
        }
        if (userDTO.getUsername() != null) {
            customUserService.updateUserFieldByIdUser("username", userDTO.getUsername(), id);
        }
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public UsersRolesDTO getUsers() {

        List<UserDTO> users = customUserService.getUserEntities().stream().map(UserDTO::getUserDTO).collect(Collectors.toList());
        List<String> roles = roleService.getRoles().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        UsersRolesDTO usersRolesDTO = new UsersRolesDTO();
        usersRolesDTO.setRoles(roles);
        usersRolesDTO.setUsers(users);

        return usersRolesDTO;
    }

    @RequestMapping(value = "/deleteUserProfile", method = RequestMethod.DELETE)
    public void deleteUserProfile(@RequestParam long id) {
        customUserService.deleteUserByIdUser(id);
    }
}
