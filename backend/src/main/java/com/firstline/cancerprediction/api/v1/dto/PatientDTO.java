package com.firstline.cancerprediction.api.v1.dto;

import lombok.Data;

import java.util.List;

@Data
public class PatientDTO {

    private long id;
    private List<DrugDTO> drugDTOList;
    private List<DiagnoseDTO> diagnoseDTOList;
    private List<ProcedureDTO> procedureDTOList;
}
