package com.firstline.cancerprediction.api.v1.controller;

import com.firstline.cancerprediction.api.v1.dto.DiagnoseDTO;
import com.firstline.cancerprediction.api.v1.dto.DrugDTO;
import com.firstline.cancerprediction.api.v1.dto.PatientDTO;
import com.firstline.cancerprediction.api.v1.dto.ProcedureDTO;
import com.firstline.cancerprediction.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/patient")
public class PatientController {

    @Autowired
    private final PatientService patientService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PatientDTO getPatientProfile(@PathVariable long id) {
        PatientDTO patientDTO = new PatientDTO();

        patientDTO.setId(id);

        patientDTO.setDiagnoseDTOList(patientService.getDiagnoseByPatientId(id).stream().map(temp -> {
            DiagnoseDTO diagnoseDTO = new DiagnoseDTO();
            diagnoseDTO.setId(temp.getId());
            diagnoseDTO.setDateStart(temp.getDateStart());
            diagnoseDTO.setDateEnd(temp.getDateEnd());
            diagnoseDTO.setDiagnose(temp.getDiagnose());
            diagnoseDTO.setPatientId(temp.getPatientId());
            return diagnoseDTO;
        }).collect(Collectors.toList()));

        patientDTO.setDrugDTOList(patientService.getDrugByPatientId(id).stream().map(temp -> {
            DrugDTO drugDTO = new DrugDTO();
            drugDTO.setId(temp.getId());
            drugDTO.setDateStart(temp.getDateStart());
            drugDTO.setDateEnd(temp.getDateEnd());
            drugDTO.setDose(temp.getDose() == null ? "0" : temp.getDose());
            drugDTO.setDrug(temp.getDrug());
            drugDTO.setPatientId(temp.getPatientId());
            return drugDTO;
        }).collect(Collectors.toList()));

        patientDTO.setProcedureDTOList(patientService.getProcedureByPatientId(id).stream().map(temp -> {
            ProcedureDTO procedureDTO = new ProcedureDTO();
            procedureDTO.setId(temp.getId());
            procedureDTO.setDateStart(temp.getDateStart());
            procedureDTO.setDateEnd(temp.getDateEnd());
            procedureDTO.setProcedure(temp.getProcedure());
            procedureDTO.setPatientId(temp.getPatientId());
            return procedureDTO;
        }).collect(Collectors.toList()));

        return patientDTO;
    }

}



