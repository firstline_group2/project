package com.firstline.cancerprediction.api.v1.dto;

import lombok.Data;

import java.util.List;

@Data
public class UsersRolesDTO {
    private List<UserDTO> users;
    private List<String> roles;
}
