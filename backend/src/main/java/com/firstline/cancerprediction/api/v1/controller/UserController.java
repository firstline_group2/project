package com.firstline.cancerprediction.api.v1.controller;

import com.firstline.cancerprediction.api.v1.dto.UserDTO;
import com.firstline.cancerprediction.entity.UserEntity;
import com.firstline.cancerprediction.service.impl.CustomUserServiceImpl;
import com.firstline.cancerprediction.service.impl.RoleServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.LinkedHashSet;
import java.util.Set;


@RestController
@RequiredArgsConstructor
@CrossOrigin()
@RequestMapping("/api/v1/user")
public class UserController {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private final CustomUserServiceImpl customUserService;

    @Autowired
    private final RoleServiceImpl roleService;

    @RequestMapping(value = "/{id}/getProfileInfo", method = RequestMethod.GET)
    public UserDTO getProfileInfo(@PathVariable long id) {

        UserEntity userEntity = customUserService.getUserEntityByIdUser(id);

        return UserDTO.getUserDTO(userEntity);
    }

    @RequestMapping(value = "/{id}/setProfileInfo", method = RequestMethod.POST)
    public ResponseEntity<Object> setProfileInfo(Principal principal, @PathVariable long id, @RequestBody @Valid UserDTO userDTO) {

        UserDTO user = UserDTO.getUserDTO(customUserService.getUserEntityByIdUser(id));

        if (principal.getName().equals(user.getUsername())) {

            if (userDTO.getPassword() != null && !userDTO.getPassword().equals("")) {
                customUserService.updateUserFieldByIdUser("password", passwordEncoder().encode(userDTO.getPassword()), id);
            }
            if (userDTO.getEmail() != null) {
                customUserService.updateUserFieldByIdUser("email", userDTO.getEmail(), id);
            }
            if (userDTO.getUsername() != null) {
                customUserService.updateUserFieldByIdUser("username", userDTO.getUsername(), id);
            }

            return ResponseEntity.ok().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/getId", method = RequestMethod.GET)
    public long getId(Principal principal){
        return customUserService.getIdUserByUsername(principal.getName());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public void registration(@RequestBody @Valid UserDTO userDTO) {
        UserEntity userEntity = new UserEntity();

        Set<GrantedAuthority> grantedAuthoritiesSet = new LinkedHashSet<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_USER");
        grantedAuthoritiesSet.add(simpleGrantedAuthority);

        userEntity.setUsername(userDTO.getUsername());
        userEntity.setPassword(passwordEncoder().encode(userDTO.getPassword()));
        userEntity.setGrantedAuthoritiesSet(grantedAuthoritiesSet);
        userEntity.setEmail(userDTO.getEmail());

        customUserService.addUserEntity(userEntity);



        for (GrantedAuthority g : userEntity.getGrantedAuthoritiesSet()) {
            roleService.addRoleByUsername(userEntity.getUsername(), g);
        }
    }
}



