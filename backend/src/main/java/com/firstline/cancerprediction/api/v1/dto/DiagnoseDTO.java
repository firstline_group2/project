package com.firstline.cancerprediction.api.v1.dto;

import com.firstline.cancerprediction.entity.common.Date;
import lombok.Data;



@Data
public class DiagnoseDTO {

    private long id;
    private Date dateStart;
    private Date dateEnd;
    private long patientId;
    private String diagnose;
}
