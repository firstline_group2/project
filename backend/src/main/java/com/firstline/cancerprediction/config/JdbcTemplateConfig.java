package com.firstline.cancerprediction.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource({"classpath:application.properties"})
public class JdbcTemplateConfig {

    @Autowired
    private Environment environment;

    @Bean(name = "hivedb")
    public DataSource dataSourceHive() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("hive.driver-class-name"));
        dataSource.setUrl(environment.getProperty("hive.url"));
        dataSource.setUsername(environment.getProperty("hive.username"));
        dataSource.setPassword(environment.getProperty("hive.password"));

        return dataSource;
    }

    @Bean(name = "mysqldb")
    public DataSource dataSourceMySQL() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("mysql.driver-class-name"));
        dataSource.setUrl(environment.getProperty("mysql.url"));
        dataSource.setUsername(environment.getProperty("mysql.username"));
        dataSource.setPassword(environment.getProperty("mysql.password"));

        return dataSource;
    }

    @Bean(name = "hiveJdbc")
    public JdbcTemplate jdbcTemplateHive(@Qualifier("hivedb") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "mysqlJdbc")
    public JdbcTemplate jdbcTemplateMySQL(@Qualifier("mysqldb") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}
