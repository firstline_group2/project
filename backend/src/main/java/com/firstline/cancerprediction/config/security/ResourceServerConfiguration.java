package com.firstline.cancerprediction.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/oauth/token", "/oauth/authorize**", "/api/v1/user/registration").permitAll();

        http
                .requestMatchers().antMatchers("/api/v1/**")
                .and()
                .authorizeRequests()
                .antMatchers("api/v1/user/**").authenticated()
                .antMatchers("/api/v1/patient/**").access("hasAnyRole('ADMIN', 'DOCTOR')")
                .antMatchers("/api/v1/admin/**").access("hasRole('ADMIN')");
    }

}