package com.firstline.cancerprediction.repository.rowmapper;

import com.firstline.cancerprediction.entity.ProcedureEntity;
import com.firstline.cancerprediction.entity.common.Date;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ProcedureRowMapper implements RowMapper<ProcedureEntity> {

    @Override
    public ProcedureEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        final ProcedureEntity procedureEntity = new ProcedureEntity();

        procedureEntity.setId(rs.getLong("id"));

        Date dateStart = new Date(rs.getString("date_start"));
        Date dateEnd = new Date(rs.getString("date_end"));

        procedureEntity.setDateStart(dateStart);
        procedureEntity.setDateEnd(dateEnd);
        procedureEntity.setPatientId(rs.getLong("patient_id"));
        procedureEntity.setProcedure(rs.getString("med_procedure"));

        return procedureEntity;
    }
}
