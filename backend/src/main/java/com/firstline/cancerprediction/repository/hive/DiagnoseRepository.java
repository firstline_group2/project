package com.firstline.cancerprediction.repository.hive;

import com.firstline.cancerprediction.entity.DiagnoseEntity;
import com.firstline.cancerprediction.repository.rowmapper.DiagnoseRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class DiagnoseRepository {

    @Autowired
    @Qualifier("hiveJdbc")
    private JdbcTemplate jdbcTemplate;

    public List<DiagnoseEntity> getDiagnoseEntitiesByPatientId(long patientId) {
        return jdbcTemplate.query("select * from diagnose where patient_id=?", new DiagnoseRowMapper(), patientId);
    }

    public void create(DiagnoseEntity diagnoseEntity) {
        jdbcTemplate.update("insert into diagnose (id, date_start, patient_id, diagnose) values (?, ?, ?, ?)",
                diagnoseEntity.getId(),
                diagnoseEntity.getDateStart().getDate(),
                diagnoseEntity.getPatientId(),
                diagnoseEntity.getDiagnose());
    }
}
