package com.firstline.cancerprediction.repository.mysql;

import com.firstline.cancerprediction.repository.rowmapper.RoleRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepository {

    @Autowired
    @Qualifier("mysqlJdbc")
    private JdbcTemplate jdbcTemplate;

    public List<GrantedAuthority> getRoles() {
        return jdbcTemplate.query("select * from role", new RoleRowMapper());
    }

    public void addRoleByIdUser(long id, GrantedAuthority grantedAuthority) {
        jdbcTemplate.update("insert into user_role (id_user, role) values (?, ?);",
                id,
                grantedAuthority.getAuthority());
    }

    public void addRoleByUsername(String username, GrantedAuthority grantedAuthority){
        jdbcTemplate.update("insert into user_role (id_user, role) values ((select id_user from users where username = ?), ?);", username, grantedAuthority.getAuthority());
    }
    public void deleteRolesByIdUser(long id) {
        jdbcTemplate.update("delete from user_role where id_user =?", id);
    }
}
