package com.firstline.cancerprediction.repository.hive;

import com.firstline.cancerprediction.entity.ProcedureEntity;
import com.firstline.cancerprediction.repository.rowmapper.ProcedureRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ProcedureRepository {

    @Autowired
    @Qualifier("hiveJdbc")
    private JdbcTemplate jdbcTemplate;

    public List<ProcedureEntity> getProcedureEntitiesByPatientId(long patientId) {
        return jdbcTemplate.query("select * from med_procedure where patient_id=?", new ProcedureRowMapper(), patientId);
    }

    public void create(ProcedureEntity procedureEntity) {
        jdbcTemplate.update("insert into med_procedure (id, date_start, date_end, patient_id, med_procedure) values (?, ?, ?, ?, ?)",
                procedureEntity.getId(),
                procedureEntity.getDateStart().getDate(),
                procedureEntity.getDateEnd().getDate(),
                procedureEntity.getPatientId(),
                procedureEntity.getProcedure());
    }
}
