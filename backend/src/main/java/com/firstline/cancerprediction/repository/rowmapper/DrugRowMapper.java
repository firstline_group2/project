package com.firstline.cancerprediction.repository.rowmapper;

import com.firstline.cancerprediction.entity.DrugEntity;
import com.firstline.cancerprediction.entity.common.Date;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class DrugRowMapper implements RowMapper<DrugEntity> {

    @Override
    public DrugEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        final DrugEntity drugEntity = new DrugEntity();

        drugEntity.setId(rs.getLong("id"));

        Date dateStart = new Date(rs.getString("date_start"));
        Date dateEnd = new Date(rs.getString("date_end"));

        drugEntity.setDateStart(dateStart);
        drugEntity.setDateEnd(dateEnd);
        drugEntity.setPatientId(rs.getLong("patient_id"));
        drugEntity.setDrug(rs.getString("drug"));
        drugEntity.setDose(rs.getString("dose"));

        return drugEntity;
    }
}
