package com.firstline.cancerprediction.repository.rowmapper;

import com.firstline.cancerprediction.entity.UserEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

public class UserRowMapper implements RowMapper<UserEntity> {

    @Override
    public UserEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        final UserEntity userEntity = new UserEntity();

        userEntity.setId(rs.getLong("id_user"));
        userEntity.setUsername(rs.getString("username"));
        userEntity.setPassword(rs.getString("password"));
        userEntity.setEmail(rs.getString("email"));

        Set<GrantedAuthority> grantedAuthoritiesSet = new LinkedHashSet<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(rs.getString("role"));
        grantedAuthoritiesSet.add(simpleGrantedAuthority);

        while (rs.next() && (rs.getLong("id_user") == userEntity.getId())){
            SimpleGrantedAuthority tempAuthority = new SimpleGrantedAuthority(rs.getString("role"));
            grantedAuthoritiesSet.add(tempAuthority);
        }
        rs.previous();

        userEntity.setGrantedAuthoritiesSet(grantedAuthoritiesSet);

        return userEntity;
    }
}
