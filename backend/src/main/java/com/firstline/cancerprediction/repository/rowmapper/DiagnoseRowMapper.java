package com.firstline.cancerprediction.repository.rowmapper;

import com.firstline.cancerprediction.entity.DiagnoseEntity;
import com.firstline.cancerprediction.entity.common.Date;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DiagnoseRowMapper implements RowMapper<DiagnoseEntity> {
    @Override
    public DiagnoseEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        final DiagnoseEntity diagnoseEntity = new DiagnoseEntity();
        diagnoseEntity.setId(rs.getLong("id"));

        Date dateStart = new Date(rs.getString("date_start"));

        diagnoseEntity.setDateStart(dateStart);
        diagnoseEntity.setDateEnd(dateStart);    //DateStart = DateEnd
        diagnoseEntity.setPatientId(rs.getLong("patient_id"));
        diagnoseEntity.setDiagnose(rs.getString("diagnose"));

        return diagnoseEntity;
    }
}
