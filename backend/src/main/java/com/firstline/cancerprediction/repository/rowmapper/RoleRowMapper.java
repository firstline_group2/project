package com.firstline.cancerprediction.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleRowMapper implements RowMapper<GrantedAuthority> {
    @Override
    public GrantedAuthority mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new SimpleGrantedAuthority(rs.getString("role"));
    }
}
