package com.firstline.cancerprediction.repository.mysql;

import com.firstline.cancerprediction.entity.UserEntity;
import com.firstline.cancerprediction.repository.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    @Qualifier("mysqlJdbc")
    private JdbcTemplate jdbcTemplate;

    public UserEntity getUserEntityByUsername(String username) {
        //long id = jdbcTemplate.queryForObject("select id_user from users where username = ?", Long.class, username);
        return jdbcTemplate.queryForObject("SELECT * FROM users INNER JOIN user_role using(id_user) where id_user = (select id_user from users where username = ?)", new UserRowMapper(), username);
    }

    public long getIdUserByUsername(String username){
        return jdbcTemplate.queryForObject("select id_user from users where username = ?", Long.class, username);
    }

    public UserEntity getUserEntityByIdUser(long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM users INNER JOIN user_role using(id_user) where id_user = ?", new UserRowMapper(), id);
    }

    public List<UserEntity> getUserEntities() {
        return jdbcTemplate.query("SELECT * FROM users INNER JOIN user_role using(id_user) ORDER BY id_user", new UserRowMapper());
    }

    public void addUserEntity(UserEntity userEntity) {
        jdbcTemplate.update("insert into users (username, password, email) values (?, ?, ?);",
                userEntity.getUsername(),
                userEntity.getPassword(),
                userEntity.getEmail());
    }

    public void updateUserFieldByIdUser(String field, String value, long id) {
        jdbcTemplate.update("update users set " + field + "=? where id_user=?", value, id);
    }

    public void deleteUserByIdUser(long id) {
        jdbcTemplate.update("delete from users where id_user =?", id);
    }

}
