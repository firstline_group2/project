package com.firstline.cancerprediction.repository.hive;

import com.firstline.cancerprediction.entity.DrugEntity;
import com.firstline.cancerprediction.repository.rowmapper.DrugRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class DrugRepository {

    @Autowired
    @Qualifier("hiveJdbc")
    private JdbcTemplate jdbcTemplate;

    public List<DrugEntity> getDrugEntitiesByPatientId(long patientId) {
        return jdbcTemplate.query("select * from drug_intake where patient_id=?", new DrugRowMapper(), patientId);
    }

    public void create(DrugEntity drugEntity) {
        jdbcTemplate.update("insert into drug_intake (id, date_start, date_end, patient_id, drug, dose) values (?, ?, ?, ?, ?, ?)",
                drugEntity.getId(),
                drugEntity.getDateStart().getDate(),
                drugEntity.getDateEnd().getDate(),
                drugEntity.getPatientId(),
                drugEntity.getDrug(),
                drugEntity.getDose());
    }
}
