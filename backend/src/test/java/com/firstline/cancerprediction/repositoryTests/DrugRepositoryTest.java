package com.firstline.cancerprediction.repositoryTests;

import com.firstline.cancerprediction.entity.DrugEntity;
import com.firstline.cancerprediction.entity.common.Date;
import com.firstline.cancerprediction.repository.hive.DrugRepository;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan({"com.firstline.cancerprediction"})
public class DrugRepositoryTest {

    private static final long DRUG_ID = 123;
    private static final Date DRUG_DATE_START = new Date("1970-01-02");
    private static final Date DRUG_DATE_END = new Date("1970-01-02");
    private static final long DRUG_PATIENT_ID = 2;
    private static final String DRUG_DRUG = "drug1";
    private static final String DRUG_DOSE = "dose1";


    @Autowired()
    private DrugRepository drugRepository;

    private DrugEntity firstDrugEntity;

    @Before
    public void setUp() {
        firstDrugEntity = new DrugEntity();
        firstDrugEntity.setId(DRUG_ID);
        firstDrugEntity.setDateStart(DRUG_DATE_START);
        firstDrugEntity.setDateEnd(DRUG_DATE_END);
        firstDrugEntity.setPatientId(DRUG_PATIENT_ID);
        firstDrugEntity.setDrug(DRUG_DRUG);
        firstDrugEntity.setDose(DRUG_DOSE);
    }

    @Test
    public void createShouldReturnValidDrugEntityWhenAddingNewDrugEntity() {
        List<DrugEntity> actual = new ArrayList<>();
        actual.add(firstDrugEntity);

        drugRepository.create(firstDrugEntity);
        List<DrugEntity> expected = drugRepository.getDrugEntitiesByPatientId(DRUG_PATIENT_ID);

        Assert.assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }
}
