package com.firstline.cancerprediction.repositoryTests;

import com.firstline.cancerprediction.entity.ProcedureEntity;
import com.firstline.cancerprediction.entity.common.Date;
import com.firstline.cancerprediction.repository.hive.ProcedureRepository;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan({"com.firstline.cancerprediction"})
public class ProcedureRepositoryTest {

    private static final long PROCEDURE_ID = 123;
    private static final Date PROCEDURE_DATE_START = new Date("1970-01-02");
    private static final Date PROCEDURE_DATE_END = new Date("1970-01-02");
    private static final long PROCEDURE_PATIENT_ID = 2;
    private static final String PROCEDURE_PROCEDURE = "procedure1";

    @Autowired()
    private ProcedureRepository procedureRepository;

    private ProcedureEntity firstProcedureEntity;

    @Before
    public void setUp() {
        firstProcedureEntity = new ProcedureEntity();
        firstProcedureEntity.setId(PROCEDURE_ID);
        firstProcedureEntity.setDateStart(PROCEDURE_DATE_START);
        firstProcedureEntity.setDateEnd(PROCEDURE_DATE_END);
        firstProcedureEntity.setPatientId(PROCEDURE_PATIENT_ID);
        firstProcedureEntity.setProcedure(PROCEDURE_PROCEDURE);
    }

    @Test
    public void createShouldReturnValidProcedureEntityWhenAddingNewProcedureEntity() {
        List<ProcedureEntity> actual = new ArrayList<>();
        actual.add(firstProcedureEntity);

        procedureRepository.create(firstProcedureEntity);
        List<ProcedureEntity> expected = procedureRepository.getProcedureEntitiesByPatientId(PROCEDURE_PATIENT_ID);

        Assert.assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }
}
