package com.firstline.cancerprediction.repositoryTests;

import com.firstline.cancerprediction.entity.DiagnoseEntity;
import com.firstline.cancerprediction.entity.common.Date;
import com.firstline.cancerprediction.repository.hive.DiagnoseRepository;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan({"com.firstline.cancerprediction"})
public class DiagnoseRepositoryTest {

    private static final long DIAGNOSE_ID = 123;
    private static final Date DIAGNOSE_DATE_START = new Date("1970-01-02");
    private static final Date DIAGNOSE_DATE_END = new Date("1970-01-02");
    private static final long DIAGNOSE_PATIENT_ID = 2;
    private static final String DIAGNOSE_DIAGNOSE = "diagnose1";

    @Autowired()
    private DiagnoseRepository diagnoseRepository;

    private DiagnoseEntity firstDiagnoseEntity;
    private DiagnoseEntity secondDiagnoseEntity;

    @Before
    public void setUp() {
        firstDiagnoseEntity = new DiagnoseEntity();
        firstDiagnoseEntity.setId(DIAGNOSE_ID);
        firstDiagnoseEntity.setDateStart(DIAGNOSE_DATE_START);
        firstDiagnoseEntity.setDateEnd(DIAGNOSE_DATE_END);
        firstDiagnoseEntity.setPatientId(DIAGNOSE_PATIENT_ID);
        firstDiagnoseEntity.setDiagnose(DIAGNOSE_DIAGNOSE);

        secondDiagnoseEntity = new DiagnoseEntity();
        secondDiagnoseEntity.setId(DIAGNOSE_ID + 1);
        secondDiagnoseEntity.setDateStart(DIAGNOSE_DATE_START);
        secondDiagnoseEntity.setDateEnd(DIAGNOSE_DATE_END);
        secondDiagnoseEntity.setPatientId(DIAGNOSE_PATIENT_ID);
        secondDiagnoseEntity.setDiagnose(DIAGNOSE_DIAGNOSE);
    }

    @Test
    public void createShouldReturnValidDiagnoseEntityWhenAddingNewDiagnoseEntity() {
        List<DiagnoseEntity> actual = new ArrayList<>();
        actual.add(firstDiagnoseEntity);
        actual.add(secondDiagnoseEntity);

        diagnoseRepository.create(firstDiagnoseEntity);
        diagnoseRepository.create(secondDiagnoseEntity);
        List<DiagnoseEntity> expected = diagnoseRepository.getDiagnoseEntitiesByPatientId(DIAGNOSE_PATIENT_ID);

        Assert.assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

}
