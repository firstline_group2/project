import React from 'react'
import checkAuthRoles from '../../containers/checkAuthRoles'
import { SERVER_URL } from '../../constants/ServerConstants'
import axios from 'axios'
import {
  ADMIN_ROLE,
  SERVER_CONNECTION_ERROR,
} from '../../constants/AppStringConstants'
import DownloadSpinner from '../DownloadSpinner'
import Table from 'react-bootstrap/Table'
import UserChangeDialog from '../UserChangeDialog'
import { Button } from 'react-bootstrap'

class AdminPanel extends React.Component {
  constructor(props) {
    super(props)
    this.getUsers = this.getUsers.bind(this)
    this.deleteUser = this.deleteUser.bind(this)

    this.state = {
      users: [],
      roles: [],
      loading: false,
      connectionError: false,
    }
  }

  componentDidMount() {
    this.getUsers()
  }

  getUsers() {
    this.setState({ loading: true })

    const url = SERVER_URL + '/api/v1/admin/users'

    const options = {
      method: 'GET',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      url,
    }

    const res = axios(options)
      .then(
        function(response) {
          let usersJson = response.data.users.map(elem => {
            return {
              id: elem.id,
              email: elem.email,
              username: elem.username,
              grantedAuthoritiesSet: elem.grantedAuthoritiesSet,
            }
          })

          this.setState({
            users: usersJson,
            loading: false,
            roles: response.data.roles,
          })
        }.bind(this)
      )
      .catch(error => {
        console.log(error)
        this.setState({ connectionError: true })
      })
    return res
  }

  deleteUser(event) {
    const url =
      SERVER_URL + '/api/v1/admin/deleteUserProfile?username=' + event.target.id

    console.log(url)

    const options = {
      method: 'DELETE',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      url,
    }

    axios(options)
      .then(function(response) {})
      .catch(error => {
        console.log(error)
        this.setState({ connectionError: true })
      })
  }

  render() {
    return (
      <div>
        {!this.state.connectionError ? (
          <div>
            {this.state.loading ? (
              <DownloadSpinner />
            ) : (
              <Table striped bordered hover variant="dark">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Roles</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.users.map(user => (
                    <tr>
                      <td>{user.id}</td>
                      <td>{user.username}</td>
                      <td>{user.email}</td>
                      <td>
                        {user.grantedAuthoritiesSet.map(role => (
                          <span key = {role}>{role} </span>
                        ))}
                      </td>
                      <td>
                        <UserChangeDialog
                          id={user.id}
                          username={user.username}
                          email={user.email}
                          userRoles={user.grantedAuthoritiesSet}
                          roles={this.state.roles}
                          history={this.props.history}
                        />
                      </td>
                      <td>
                        <Button
                          id={user.id}
                          variant="primary"
                          onClick={this.deleteUser}
                        >
                          Удалить
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            )}
          </div>
        ) : (
          <div>{SERVER_CONNECTION_ERROR}</div>
        )}
      </div>
    )
  }
}

export default checkAuthRoles(AdminPanel, [ADMIN_ROLE], true, '/signin')
