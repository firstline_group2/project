import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import LoginButton from '../Buttons/LoginButton'
import {
  ADMIN_PANEL_LINK,
  ADMIN_ROLE,
  DOCTOR_ROLE,
  MAIN_PAGE,
  PATIENT_PROFILE_LINK, USER_PROFILE_LINK, USER_ROLE,
} from '../../constants/AppStringConstants'
import checkAuthRoles from '../../containers/checkAuthRoles'

const UlNav = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  text-align: center;
}

`

const LiNav = styled.li`
  display: inline-block;
  margin-right: -4px;
  @media (max-width: 300px) {
    display: block;
  }
`

export const NavLinkCSS = styled(NavLink)`
  text-decoration: none;
  display: inline-block;
  padding: 15px 30px;
  font-family: 'PT Sans Caption', sans-serif;
  color: white;
  transition: 0.3s linear;
  &:hover {
    background: rgba(0, 0, 0, 0.2);
  }
`

const PatientProfileLink = () => (
  <NavLinkCSS activeClassName="active" to="/patient">
    {PATIENT_PROFILE_LINK}
  </NavLinkCSS>
)

const PatientProfileWithAccessLink = checkAuthRoles(PatientProfileLink, [
  ADMIN_ROLE,
  DOCTOR_ROLE,
])

const AdminPanelLink = () => (
  <NavLinkCSS activeClassName="active" to="/admin">
    {ADMIN_PANEL_LINK}
  </NavLinkCSS>
)

const AdminPanelWithAccessLink = checkAuthRoles(AdminPanelLink, [ADMIN_ROLE])


const UserProfileLink = () => (
  <NavLinkCSS activeClassName="active" to="/profile">
    {USER_PROFILE_LINK}
  </NavLinkCSS>
)

const UserProfileWithAccessLink = checkAuthRoles(UserProfileLink, [USER_ROLE,ADMIN_ROLE,DOCTOR_ROLE])

export default function Navigation() {
  return (
    <UlNav>
      <LiNav>
        <NavLinkCSS activeClassName="active" to="/main">
          {MAIN_PAGE}
        </NavLinkCSS>
      </LiNav>
      <LiNav>
        <PatientProfileWithAccessLink />
      </LiNav>
      <LiNav>
        <AdminPanelWithAccessLink />
      </LiNav>
      <LiNav>
        <UserProfileWithAccessLink/>
      </LiNav>
      <LiNav>
        <LoginButton />
      </LiNav>
    </UlNav>
  )
}
