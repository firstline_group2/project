import React from 'react'
import withAuthProps from '../../containers/withAuthProps'
import { SERVER_URL } from '../../constants/ServerConstants'
import axios from 'axios'
import Form from 'react-bootstrap/Form'
import { Button } from 'react-bootstrap'

class UserProfile extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      login: '',
      email: '',
      password: '',
      roles: '',
      connectionError: 'false',
    }
  }

  getUserProfile = () => {
    const url = SERVER_URL + '/api/v1/user/' + this.props.id + '/getProfileInfo'
    console.log(url)

    const options = {
      method: 'GET',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      url,
    }

    axios(options)
      .then(res => {
        console.log(res)
        const login = res.data.username
        const email = res.data.email
        const roles = res.data.grantedAuthoritiesSet

        this.setState({
          login: login,
          email: email,
          roles: roles,
        })
      })
      .catch(error => {
        console.log(error)
        this.setState({ connectionError: true })
      })
  }

  setUserProfile = () => {
    const url = SERVER_URL + '/api/v1/user/' + this.props.id + '/setProfileInfo'
    console.log(url)

    const data = {
      email: this.state.email,
      password: this.state.password,
      username: this.state.login,
      grantedAuthoritiesSet: this.state.roles,
    }

    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      data: data,
      url,
    }

    axios(options)
      .then(res => {
        console.log(res)
        const login = res.data.username
        const email = res.data.email
        const roles = res.data.grantedAuthoritiesSet

        this.setState({
          login: login,
          email: email,
          roles: roles,
        })
        console.log('saved')
      })
      .catch(error => {
        console.log(error)
        this.setState({ connectionError: true })
      })
  }

  handleChange = event => {
    console.log(event.target.id + ' ' + event.target.value)
    this.setState({
      [event.target.id]: event.target.value,
    })
  }

  handleSubmit = event => {
    this.setUserProfile()
    event.preventDefault()
  }

  componentDidMount() {
    this.getUserProfile()
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Group controlId="login">
          <Form.Label>Login</Form.Label>
          <Form.Control
            type="login"
            placeholder="Введите имя пользователя"
            value={this.state.login}
            onChange={this.handleChange}
          />
        </Form.Group>

        <Form.Group controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Введите ваш email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Новый пароль"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </Form.Group>

        <Form.Group controlId="roles">
          <Form.Label>Roles</Form.Label>
          <Form.Control
            type="roles"
            readOnly
            value={this.props.roles.map(elem => {
              return elem
            })}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Save
        </Button>
      </Form>
    )
  }
}

export default withAuthProps(UserProfile)
