import React from 'react'
import { Redirect } from 'react-router'
import { createOauthFlow } from '../../react-oauth-flow/createOauthFlow'
import { connect } from 'react-redux'
import { setAuth } from '../../store/auth/actions'
import DownloadSpinner from '../DownloadSpinner'

const { Receiver } = createOauthFlow({
  authorizeUrl: 'https://www.dropbox.com/oauth2/authorize',
  tokenUrl: 'https://api.dropboxapi.com/oauth2/token',
  clientId: process.env.REACT_APP_DB_KEY,
  clientSecret: process.env.REACT_APP_DB_SECRET,
  redirectUri: 'http://localhost:82/signin/callback',
})

class Callback extends React.Component {
  constructor(props) {
    super(props)
    this.handleSuccess = this.handleSuccess.bind(this)
  }

  handleSuccess = (accessToken, { response, state }) => {
    // console.log('Success!')
    // console.log('AccessToken: ', accessToken)
    // console.log('Response: ', response)
    // console.log('State: ', state)
    this.props.dispatch(setAuth(true))
    localStorage.setItem('s_token', accessToken)
  }

  render() {
    return <Receiver
      state={{ to: '/' }}
      onAuthSuccess={this.handleSuccess}
      render={({ processing, state }) => {
        if (processing) {
          return <DownloadSpinner />
        }

        return <Redirect to={state.to} />
      }}
    />
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.authReducer.isAuth,
  }
}

export default connect(mapStateToProps)(Callback)
