import React from 'react'
import Header from '../AppStructure/Header'
import Footer from '../AppStructure/Footer'
import ContentRouter from '../AppStructure/ContentRouter'

import { BrowserRouter } from 'react-router-dom'
import {
  setAuth,
  setUserId,
  setUserName,
  setUserRole,
} from '../../store/auth/actions'
import withAuthProps from '../../containers/withAuthProps'
import styled from 'styled-components'

const AppWrapperCss = styled.div`
  a:hover {
    text-decoration: none;
    color: white;
  }
`

class App extends React.Component {
  checkAuth = () => {
    const token = localStorage.getItem('jwt_token')
    const userId = localStorage.getItem('user_id')
    if (token && userId) {
      const jwtDecode = require('jwt-decode')
      const decoded = jwtDecode(token)
      this.props.dispatch(setAuth(true))
      this.props.dispatch(setUserName(decoded.user_name))
      this.props.dispatch(setUserRole(decoded.authorities))
      this.props.dispatch(setUserId(userId))

      console.log(decoded)
      console.log(userId)
    }
  }

  componentWillMount() {
    this.checkAuth()
  }
  render() {
    return (
      <BrowserRouter>
        <AppWrapperCss>
          <div className="App">
            <Header />
            <ContentRouter />
            <Footer />
          </div>
        </AppWrapperCss>
      </BrowserRouter>
    )
  }
}

export default withAuthProps(App)
