import React from 'react'
import {
  Alert,
  Button,
  Form,
  FormControl,
  InputGroup,
  Modal,
} from 'react-bootstrap'
import { SERVER_URL } from '../../constants/ServerConstants'
import axios from 'axios'

class UserChangeDialog extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      username: props.username,
      email: props.email,
      password: '',
      isPasswordChanged: false,
      userRoles: props.userRoles,
      roles: props.roles,
      show: false,
      alertShow: false,
    }

    this.handleShow = this.handleShow.bind(this)
    this.showAlert = this.showAlert.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleCloseAlert = this.handleCloseAlert.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.saveUser = this.saveUser.bind(this)
  }

  handleShow() {
    this.setState({ show: true })
  }

  showAlert() {
    this.setState({ showAlert: true })
  }

  handleClose() {
    this.setState({ show: false })
  }

  handleCloseAlert() {
    this.setState({ showAlert: false })
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    })
    console.log(event.target.id)
  }

  handleChangePassword = event => {
    if (event.target.value.length === 0) {
      this.setState({
        isPasswordChanged: false,
        password: undefined,
      })
    } else {
      this.setState({
        isPasswordChanged: true,
      })
    }
    this.setState({
      password: event.target.value,
    })
  }

  saveUser() {
    const data = {
      email: this.state.email,
      username: this.state.username,
      grantedAuthoritiesSet: this.state.userRoles,
      password: this.state.password,
    }

    const url =
      SERVER_URL +
      '/api/v1/admin/setUserProfile?id=' +
      this.props.id

    console.log(url)

    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      data: data,
      url,
    }

    axios(options).then(
      function(response) {
        console.log('saved')
        this.props.history.push('/admin')
        this.setState({ showAlert: true })
      }.bind(this)
    )
  }

  render() {
    return (
      <>
        <Button variant="primary" onClick={this.handleShow}>
          Редактировать
        </Button>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Редактирование данных</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <InputGroup className="mb-3">
              <FormControl
                id="username"
                placeholder="Username"
                aria-label="Username"
                value={this.state.username}
                onChange={this.handleChange}
              />
            </InputGroup>

            <InputGroup className="mb-3">
              <FormControl
                id="email"
                placeholder="email"
                aria-label="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </InputGroup>

            <InputGroup className="mb-3">
              <FormControl
                id="password"
                placeholder="password"
                aria-label="password"
                onChange={this.handleChangePassword}
              />
            </InputGroup>

            <InputGroup className="mb-3">
              {this.state.roles.map(elem => (
                <Form.Check
                  key={elem}
                  id={elem}
                  inline
                  label={elem}
                  type="checkbox"
                  defaultChecked={this.props.userRoles.some(
                    role => role === elem
                  )}
                  onChange={e => {
                    console.log('before ' + this.state.userRoles)

                    if (this.state.userRoles.indexOf(e.target.id) !== -1) {
                      let array = [...this.state.userRoles]
                      let index = array.indexOf(e.target.id)

                      array.splice(index, 1)
                      this.setState({ userRoles: array })

                      console.log('deleted ' + e.target.id)
                    } else {
                      let array = [...this.state.userRoles]
                      this.setState({ userRoles: [...array, e.target.id] })
                      console.log('added ' + e.target.id)
                    }
                  }}
                />
              ))}
            </InputGroup>

            <>
              <Alert show={this.state.alertShow} variant="success">
                <Alert.Heading>How's it going?!</Alert.Heading>
                <p>Изменения сохранены</p>
                <hr />
                <div className="d-flex justify-content-end">
                  <Button onClick={() => false} variant="outline-success">
                    Закрыть
                  </Button>
                </div>
              </Alert>
            </>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Закрыть
            </Button>
            <Button variant="primary" onClick={this.saveUser}>
              Сохранить
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
  }
}

export default UserChangeDialog
