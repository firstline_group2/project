import React from 'react'
import styled from 'styled-components'

const FooterCSS = styled.div`
  background-color: rgb(54, 56, 55);
  min-height: 10vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  position: relative;
  z-index: 1000;
`

export default function Footer() {
  return (
    <FooterCSS>
      <span>
        © Ф-Лайн Софтвер
      </span>
    </FooterCSS>
  )
}
