import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import AppInfo from '../../AppInfo/index'
import styled from 'styled-components'
import Callback from '../../Callback'
import SignInForm from '../../Forms/SignInForm'
import SignUpForm from '../../Forms/SignUpForm'
import PatientProfile from '../../PatientProfile'
import AdminPanel from '../../AdminPanel'
import UserProfile from '../../UserProfile'

const ContentCSS = styled.div`
  background-color: rgba(0, 0, 0, 0);
  min-height: 75vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: #323641;
  position: relative;
  z-index: 1000;
  text-align: center;
  margin: auto;
`

export default function ContentRouter() {
  return (
    <ContentCSS>
      <Switch>
        <Route exact path="/main" component={AppInfo} />
        <Route path="/patient" component={PatientProfile} />
        <Route path="/admin" component={AdminPanel} />
        <Route exact path="/signin" component={SignInForm} />
        <Route exact path="/signin/callback" component={Callback} />
        <Route exact path="/signup" component={SignUpForm} />
        <Route exact path="/profile" component={UserProfile} />
        <Redirect to="/main" />
      </Switch>
    </ContentCSS>
  )
}
