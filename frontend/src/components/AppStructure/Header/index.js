import React from 'react'
import styled from 'styled-components'
import Navigation from '../../Navigation/index'

const HeaderCSS = styled.div`
  background-color: rgb(54, 56, 55);
  min-height: 15vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  position: relative;
  z-index: 1000;
`

function Header() {
  return (
    <HeaderCSS className="App-header">
      <Navigation />
    </HeaderCSS>
  )
}

export default Header
