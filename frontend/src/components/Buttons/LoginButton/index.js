import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { setAuth } from '../../../store/auth/actions'
import { SIGNIN, SIGNOUT } from '../../../constants/AppStringConstants'
import { withRouter } from 'react-router'

const LoginButtonCSS = styled(NavLink)`
  text-decoration: none;
  display: inline-block;
  padding: 15px 30px;
  font-family: 'PT Sans Caption', sans-serif;
  color: white;
  transition: 0.3s linear;
  background: rgb(32, 216, 205);
  &:hover {
    background: rgb(216, 24, 84);
  }
`
const LoginButton = ({ isAuth,history, ...props }) => {
  const signOut = () => {
    props.dispatch(setAuth(false))
    localStorage.setItem('s_token', '')
  }

  const signIn = () => {
    history.push('/auth')
  }

  return (
    <div className={'main-login-button'}>
      {isAuth ? (
        <LoginButtonCSS onClick={signOut} activeClassName="active" to="/main">
          {SIGNOUT}
        </LoginButtonCSS>
      ) : (
        <LoginButtonCSS onClick={signIn} activeClassName="active" to="/signin">
          {SIGNIN}
        </LoginButtonCSS>
      )}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isAuth: state.authReducer.isAuth,
  }
}

const LoginButtonWithRouter = withRouter(LoginButton)


export default connect(mapStateToProps)(LoginButtonWithRouter)
