import { createOauthFlow } from '../../../react-oauth-flow/createOauthFlow/index'
import React from 'react'
import styled from 'styled-components'

const { Sender } = createOauthFlow({
  authorizeUrl: 'https://www.dropbox.com/oauth2/authorize',
  tokenUrl: 'https://api.dropboxapi.com/oauth2/token',
  clientId: process.env.REACT_APP_DB_KEY,
  clientSecret: process.env.REACT_APP_DB_SECRET,
  redirectUri: 'http://localhost:82/signin/callback',
})

const DropBoxButtonCSS = styled.div`
  a {
    color: white;
  }
`

class DropboxLoginButton extends React.Component {
  render() {
    return (
      <DropBoxButtonCSS>
        <Sender
          state={{ to: '/auth/success' }}
          render={({ url }) => <a href={url}>Dropbox</a>}
        />
      </DropBoxButtonCSS>
    )
  }
}

export default DropboxLoginButton
