import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import './Signup.css'
import Form from 'react-bootstrap/Form'
import axios from 'axios'
import { SERVER_URL } from '../../../constants/ServerConstants'
import withAuthProps from '../../../containers/withAuthProps'

class SignUpForm extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)

    this.state = {
      email: '',
      login: '',
      password: '',
      confirmPassword: '',
    }
  }

  validateForm() {
    return (
      this.state.login.length > 0 &&
      this.state.password.length > 0 &&
      this.state.email.length &&
      this.state.password === this.state.confirmPassword
    )
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    })
  }

  handleSubmit = event => {
    const email = event.target.elements[0].value
    const login = event.target.elements[1].value
    const password = event.target.elements[2].value

    const url = SERVER_URL + '/api/v1/user/registration'

    const data = { username: login, email: email, password: password }
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization: 'Basic Y2xpZW50OnBhc3N3b3Jk',
      },
      data: data,
      url,
    }
    axios(options).then(res => {})

    event.preventDefault()
  }

  render() {
    return (
      <div className="SignUp">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="login">
            <Form.Label>Login</Form.Label>
            <Form.Control
              autoFocus
              type="login"
              value={this.state.login}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Form.Group controlId="confirmPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              value={this.state.confirmPassword}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Button block type="submit" disabled={!this.validateForm()}>
            SignUp
          </Button>
        </Form>
      </div>
    )
  }
}

export default withAuthProps(SignUpForm)
