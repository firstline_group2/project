import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import './Signin.css'
import Form from 'react-bootstrap/Form'
import DropboxLoginButton from '../../Buttons/DropboxLoginButton'
import axios from 'axios'
import * as qs from 'qs'
import { setAuth, setUserId, setUserName, setUserRole } from '../../../store/auth/actions'
import { SERVER_URL } from '../../../constants/ServerConstants'
import withAuthProps from '../../../containers/withAuthProps'

class SignInForm extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)

    this.state = {
      login: '',
      password: '',
    }
  }

  validateForm() {
    return this.state.login.length > 0 && this.state.password.length > 0
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    })
  }

  handleSubmit = event => {
    console.log('signin')
    const login = event.target.elements[0].value
    const password = event.target.elements[1].value
    const url = SERVER_URL + '/oauth/token'
    console.log(url)
    const data = { password: password, username: login, grant_type: 'password' }
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: 'Basic Y2xpZW50OnBhc3N3b3Jk',
      },
      data: qs.stringify(data),
      url,
    }
    axios(options).then(res => {
      console.log('all ok')
      localStorage.setItem('jwt_token', res.data.access_token)

      const token = localStorage.getItem('jwt_token')
      if (token) {
        const jwtDecode = require('jwt-decode')
        const decoded = jwtDecode(token)
        this.props.dispatch(setAuth(true))
        this.props.dispatch(setUserName(decoded.user_name))
        this.props.dispatch(setUserRole(decoded.authorities))
      }

      const url = SERVER_URL + '/api/v1/user/getId'
      console.log(url)

      const options = {
        method: 'GET',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          authorization: 'Bearer ' + token,
        },
        url,
      }

      axios(options)
        .then(res => {
          const userId = res.data
          console.log(userId)
          console.log('id saved')
          localStorage.setItem('user_id', userId)
          this.props.dispatch(setUserId(userId))
        })
        .catch(error => {
          console.log(error)
          this.setState({ connectionError: true })
        })

      this.props.history.push('/')
    }).catch((error) => {
      console.log(error)
    })
    event.preventDefault()
  }

  render() {
    return (
      <div className="SignIn">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="login">
            <Form.Label>login</Form.Label>
            <Form.Control
              autoFocus
              type="login"
              value={this.state.login}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Button block type="submit" disabled={!this.validateForm()}>
            Login
          </Button>
          <Button>
            <DropboxLoginButton />
          </Button>
          <Button onClick={()=> this.props.history.push('/signup')}>
            Регистрация
          </Button>
        </Form>
      </div>
    )
  }
}

export default withAuthProps(SignInForm)
