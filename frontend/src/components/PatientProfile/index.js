import React, { Component } from 'react'
import axios from 'axios'
import { Chart } from 'react-google-charts'

import DownloadSpinner from '../DownloadSpinner/index'
import { SERVER_URL } from '../../constants/ServerConstants'
import checkAuthRoles from '../../containers/checkAuthRoles'
import {
  ADMIN_ROLE,
  DOCTOR_ROLE,
  DOWNLOAD_CHART_ERROR_MSG,
} from '../../constants/AppStringConstants'

class PatientProfile extends Component {
  getData = patientId => {
    const url = SERVER_URL + '/api/v1/patient/' + patientId
    const options = {
      method: 'GET',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        authorization: 'Bearer ' + localStorage.getItem('jwt_token'),
      },
      url,
    }

    const res = axios(options)
      .then(
        function(response) {
          if (response.data.drugDTOList.length === 0) {
            this.setState({ isDrugEmpty: true })
          } else {
            let drugJson = response.data.drugDTOList.map(elem => {
              return [
                elem.drug,
                elem.dose,
                new Date(
                  elem.dateStart.year,
                  elem.dateStart.month,
                  elem.dateStart.day,
                  0,
                  0,
                  0
                ),
                new Date(
                  elem.dateEnd.year,
                  elem.dateEnd.month,
                  elem.dateEnd.day,
                  0,
                  0,
                  1
                ),
              ]
            })
            drugJson = [
              [
                { type: 'string', id: 'Drug' },
                { type: 'string', id: 'Dose' },
                { type: 'date', id: 'Start' },
                { type: 'date', id: 'End' },
              ],
              ...drugJson,
            ]
            this.setState({
              drugs: drugJson,
            })
            console.log(drugJson)
          }
          if (response.data.procedureDTOList.length === 0) {
            this.setState({ isProcedureEmpty: true })
          } else {
            let procedureJson = response.data.procedureDTOList.map(elem => {
              return [
                elem.procedure,
                new Date(
                  elem.dateStart.year,
                  elem.dateStart.month,
                  elem.dateStart.day,
                  0,
                  0,
                  0
                ),
                new Date(
                  elem.dateEnd.year,
                  elem.dateEnd.month,
                  elem.dateEnd.day,
                  0,
                  0,
                  1
                ),
              ]
            })
            procedureJson = [
              [
                { type: 'string', id: 'Procedure' },
                { type: 'date', id: 'Start' },
                { type: 'date', id: 'End' },
              ],
              ...procedureJson,
            ]
            this.setState({
              procedures: procedureJson,
            })
            console.log(procedureJson)
          }
          if (response.data.diagnoseDTOList.length === 0) {
            this.setState({ isDiagnoseEmpty: true })
          } else {
            let diagnoseJson = response.data.diagnoseDTOList.map(elem => {
              return [
                elem.diagnose,
                new Date(
                  elem.dateStart.year,
                  elem.dateStart.month,
                  elem.dateStart.day,
                  0,
                  0,
                  0
                ),
                new Date(
                  elem.dateEnd.year,
                  elem.dateEnd.month,
                  elem.dateEnd.day,
                  0,
                  0,
                  1
                ),
              ]
            })
            diagnoseJson = [
              [
                { type: 'string', id: 'diagnose' },
                { type: 'date', id: 'Start' },
                { type: 'date', id: 'End' },
              ],
              ...diagnoseJson,
            ]
            this.setState({
              diagnoses: diagnoseJson,
            })
            console.log(diagnoseJson)
          }

          this.setState({
            loading: false,
          })

        }.bind(this)
      )
      .catch(error => {
        console.log(error)
        this.setState({
          downloadChartDataError: true,
        })
      })
    return res
  }

  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getData = this.getData.bind(this)
    this.state = {
      drugs: [],
      procedures: [],
      diagnoses: [],
      loading: false,
      isShowChart: false,
      downloadChartDataError: false,
      isDrugEmpty: false,
      isProcedureEmpty: false,
      isDiagnoseEmpty: false,
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const patientId = e.target.elements[0].value
    this.props.history.push('/patient/' + patientId)

    this.setState({
      drugs: [],
      procedures: [],
      diagnoses: [],
      loading: false,
      isShowChart: false,
      downloadChartDataError: false,
      isDrugEmpty: false,
      isProcedureEmpty: false,
      isDiagnoseEmpty: false,
    })

    this.setState({
      loading: true,
      isShowChart: true,
    })

    this.getData(patientId)
  }

  render() {
    return (
      <div>
        <div className="patient-form">
          <div className="col-md-12">ID пациента:</div>
          <form className="col-md-12" onSubmit={this.handleSubmit}>
            <input className="col-md-6" type="number" placeholder="ID" />
            <button className="col-md-6" type="submit">Выбрать</button>
          </form>
        </div>
        <div className={'patient-charts-container'}>
          {this.state.isShowChart && (
            <div>
              {this.state.loading ? (
                <div>
                  {!this.state.downloadChartDataError ? (
                    <DownloadSpinner />
                  ) : (
                    <div>{DOWNLOAD_CHART_ERROR_MSG}</div>
                  )}
                </div>
              ) : (
                <div className={'patientCharts'}>
                  {!this.state.isDrugEmpty && (
                    <div>
                      <div>Лекарства</div>
                    <Chart
                      width={'500px'}
                      height={'25vh'}
                      chartType="Timeline"
                      loader={<DownloadSpinner />}
                      data={this.state.drugs}
                      options={{
                        showRowNumber: true,
                      }}
                      rootProps={{ 'data-testid': '1' }}
                    />
                    </div>
                  )}
                  {!this.state.isProcedureEmpty && (
                    <div>
                      <div>Процедуры</div>
                    <Chart
                      width={'500px'}
                      height={'25vh'}
                      chartType="Timeline"
                      loader={<DownloadSpinner />}
                      data={this.state.procedures}
                      options={{
                        showRowNumber: true,
                      }}
                      rootProps={{ 'data-testid': '1' }}
                    />
                    </div>
                  )}
                  {!this.state.isDiagnoseEmpty && (
                    <div>
                      <div>Диагнозы</div>
                    <Chart
                      width={'500px'}
                      height={'25vh'}
                      chartType="Timeline"
                      loader={<DownloadSpinner />}
                      data={this.state.diagnoses}
                      options={{
                        showRowNumber: true,
                      }}
                      rootProps={{ 'data-testid': '1' }}
                    />
                    </div>
                  )}
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default checkAuthRoles(
  PatientProfile,
  [DOCTOR_ROLE, ADMIN_ROLE],
  true,
  '/signin'
)
