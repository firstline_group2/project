export const SET_AUTH = 'SET_AUTH'
export const SET_USER_NAME = 'SET_USER_NAME'
export const SET_USER_ID = 'SET_USER_ID'
export const SET_USER_ROLE = 'SET_USER_ROLE'

export const setAuth = boolAuth => {
  return {
    type: SET_AUTH,
    payload: boolAuth,
  }
}
export const setUserName = name => {
  return {
    type: SET_USER_NAME,
    payload: name,
  }
}

export const setUserId = id => {
  return {
    type: SET_USER_ID,
    payload: id,
  }
}
export const setUserRole = role => {
  return {
    type: SET_USER_ROLE,
    payload: role,
  }
}
