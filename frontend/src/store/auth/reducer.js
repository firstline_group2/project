import { SET_AUTH, SET_USER_ID, SET_USER_NAME, SET_USER_ROLE } from './actions'
import {
  ADMIN_ROLE,
  DOCTOR_ROLE,
  USER_ROLE,
} from '../../constants/AppStringConstants'

const initialState = {
  isAuth: false,
  login: '',
  roles: [ADMIN_ROLE, USER_ROLE, DOCTOR_ROLE],
  id: '',
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTH:
      return { ...state, isAuth: action.payload }
    case SET_USER_NAME:
      return { ...state, login: action.payload }
    case SET_USER_ROLE:
      return { ...state, roles: action.payload }
    case SET_USER_ID:
      return { ...state, id: action.payload }
    default:
      return state
  }
}
