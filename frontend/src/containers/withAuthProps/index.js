import React from 'react'
import { connect } from 'react-redux'

const withAuthProps = Component =>
  connect(mapStateToProps)(
    class WithAuthRoles extends React.Component {
      render() {
        return (
          <div>
            <Component {...this.props} />
          </div>
        )
      }
    }
  )

const mapStateToProps = state => {
  return {
    id: state.authReducer.id,
    isAuth: state.authReducer.isAuth,
    roles: state.authReducer.roles,
    login: state.authReducer.login,
  }
}

export default withAuthProps
