import React from 'react'
import { Redirect } from 'react-router'
import withAuthProps from '../withAuthProps'

const checkAuthRoles = (
  Component,
  accessRoles,
  redirected = false,
  redirect = '#'
) =>
  withAuthProps(
    class CheckAuthProps extends React.Component {
      constructor(props) {
        super(props)
        this.checkRoles = this.checkRoles.bind(this)
      }

      checkRoles() {
        const roles = this.props.roles

        const accessBool = roles.some((role)=>{
          return (accessRoles.indexOf(role) !== -1)
        })

        return accessBool
      }

      render() {
        return (
          <div>
            {this.props.isAuth && this.checkRoles() ? (
              <Component {...this.props} />
            ) : (
              <div>{redirected && <Redirect to={redirect} />}</div>
            )}
          </div>
        )
      }
    }
  )

export default checkAuthRoles
