# Спецификация
_________________
## Спецификация форматов обмена

### Формат обмена ядро\back end

обмен через запрос запрос к таблицам

### Формат обмена back end\front end
Json

```sh
diagnose :
{
    "ID" : bigint,
    "Date_start": {
        "year": int,
        "month": int,
        "day": int,
    },
    "Patient_ID": bigint,
    "Diagnose": string
}
```
```sh
med_procedure :
{
    "ID" : bigint,
    "Date_start": {
        "year": int,
        "month": int,
        "day": int,
    },
    "Date_end":{
        "year": int,
        "month": int,
        "day": int,
    },
    "Patient_ID": bigint,
    "Procedure": string
}
```
```sh
drug_intake :
{
    "ID" : bigint,
    "Date_start": {
        "year": int,
        "month": int,
        "day": int,
    },
    "Date_end": {
        "year": int,
        "month": int,
        "day": int,
    },
    "Patient_ID": bigint,
    "Drug": string,
    "Dose": string
}
```
### Спецификация формата внутренней модели

```
diagnose(
    ID     BIGINT,
    date_start      DATE,
    patient_id      BIGINT,
    diagnose        STRING)
```
```
med_procedure(
    ID     BIGINT,
    date_start      DATE,
    date_end        DATE,
    patient_id      BIGINT,
    med_procedure   STRING,
    dose        STRING)
```
```
drug_intake(
    ID     BIGINT,
    date_start      DATE,
    date_end        DATE,
    patient_id      BIGINT,
    drug        STRING,
    dose        STRING)
```

###  Спецификация формата ML-модели

```
ml_table(
//входные данные
    patient_id BIGINT,
    drug     STRING,
    drug_time INT,
    drug_intake_gap      INT,
    med_procedure        STRING,
//выходное значение
    stage        STRING)
```


